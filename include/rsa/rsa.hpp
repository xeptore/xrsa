#include <string>

namespace xeptore
{
    void setPublic();
    std::string encrypt(std::string num);
} // namespace xeptore
